FROM node:14.11.0-alpine

WORKDIR /app


ENV PATH /app/node_modules/.bin:$PATH

COPY package.json /app
RUN npm install --silent
RUN npm install react-scripts@3.4.1 -g --silent

RUN yarn install

COPY . /app

CMD ["npm", "start", "app.js"]